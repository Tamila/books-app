require 'test_helper'

class AuthorBookTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @author = authors(:salvatore)
    @book = books(:maestro)
    @author_book = AuthorBook.new(author: @author, book: @book)
  end

  test "should be valid" do
    assert @author_book.valid?
  end

  test "author should be present" do
    @author_book.author = nil
    assert_not @author_book.valid?
  end

  test "book should be present" do
    @author_book.book = nil
    assert_not @author_book.valid?
  end

  test "author book count changes with new book" do
    @author_book.save
    assert_equal @author.reload.books.count, 2
  end
end
