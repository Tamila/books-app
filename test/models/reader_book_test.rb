require 'test_helper'

class ReaderBookTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = Reader.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    @reader.save
    @book = books(:hero)
    @reader_book = ReaderBook.new(reader: @reader, book: @book, status: 'Read')
    @author = authors(:salvatore)
  end

  test "should be valid" do
    assert @reader_book.valid?
  end

  test "reader should be present" do
    @reader_book.reader = nil
    assert_not @reader_book.valid?
  end

  test "book should be present" do
    @reader_book.book = nil
    assert_not @reader_book.valid?
  end

  test "status should be present" do
    @reader_book.status = nil
    assert_not @reader_book.valid?
  end

  test "valid status accepted" do
    @reader_book.status = 'Reading'
    assert @reader_book.valid?
    @reader_book.status = 'Want to read'
    assert @reader_book.valid?
  end

  test "rating should start from 0" do
    assert_equal 0, @reader_book.rating
  end

  test "rating amount and average should change if rating added" do
    @reader_book = ReaderBook.new(reader: @reader, book: @book, 
                                  status: 'Read', rating: 5)
    @reader_book.save
  
    assert_equal @book.reload.rating_average, 4.0
    assert_equal @book.reload.rating_amount, 3 
    assert_equal @author.reload.rating_average, 4.0
  end

  test "rating amount and average shouldn't change if rating 0" do
    assert_no_difference '@book.rating_average' do
      assert_no_difference '@book.rating_amount' do
        @reader_book = ReaderBook.new(reader: @reader, book: @book, 
                                      status: 'Read', rating: 0)
        @reader_book.save
      end
    end
  end

end