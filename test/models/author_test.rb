require 'test_helper'

class AuthorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
    	website: "http://www.example.com", bio: "born somewhere sometime...")
  end

  test "should be valid" do
    assert @author.valid?
  end

  test "name should be present" do
    @author.name = "     "
    assert_not @author.valid?
  end

  test "name should not be too long" do
    @author.name = "a" * 101
    assert_not @author.valid?
  end

  test "should be valid without url" do
    @author.website = ""
    assert @author.valid?
  end

  test "url validation should accept valid urls" do
    valid_urls = %w[https://www.railstutorial.org/book http://www.ling.org.ua 
    	http://foo.bar.org/A_US-ER http://www.first.last/foo/jp http://www.alice+bob.cn/baz]
    valid_urls.each do |valid_url|
      @author.website = valid_url
      assert @author.valid?, "#{valid_url.inspect} should be valid"
    end
  end

  test "url validation should reject invalid urls" do
    invalid_urls = %w[userexample,com user_at_fooorg user.name/example.
                           foo/bar_baz.com http/bar+baz.com]
    invalid_urls.each do |invalid_url|
      @author.website = invalid_url
      assert_not @author.valid?, "#{invalid_url.inspect} should be invalid"
    end
  end

  test "default average rating is 0" do
    @author.save
    assert_equal @author.reload.rating_average, 0
  end
end
