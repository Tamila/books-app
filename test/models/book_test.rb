require 'test_helper'

class BookTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @book = Book.new(name: "Example book", year: 2000,
    	ISBN: "65675456754", genre: "Fantasy")
  end

  test "should be valid" do
    assert @book.valid?
  end

  test "name should be present" do
    @book.name = "     "
    assert_not @book.valid?
  end

  test "name should not be too long" do
    @book.name = "a" * 256
    assert_not @book.valid?
  end

  test "year should be >=1000" do
    @book.year = 990
    assert_not @book.valid?
  end

  test "year should be <=Date.today.year" do
    @book.year = Date.today.year+1
    assert_not @book.valid?
  end

  test "ISBN should be unique" do
    duplicate_book = @book.dup
    @book.save
    assert_not duplicate_book.valid?
  end

  test "default average rating is 0" do
    @book.save
    assert_equal @book.reload.rating_average, 0
  end

  test "default rating number is 0" do
    @book.save
    assert_equal @book.reload.rating_amount, 0
  end
end
