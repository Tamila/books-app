require 'test_helper'

class RecommendationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = Reader.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    @reader.save
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    @author_book = Recommendation.new(reader: @reader, book: @book)
  end

  test "should be valid" do
    assert @author_book.valid?
  end

  test "reader should be present" do
    @author_book.reader = nil
    assert_not @author_book.valid?
  end

  test "book should be present" do
    @author_book.book = nil
    assert_not @author_book.valid?
  end

end
