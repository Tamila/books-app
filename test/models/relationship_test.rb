require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @relationship = Relationship.new(follower_id: readers(:michael).id,
                                     followed_id: readers(:archer).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "follower should be present" do
  	@relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "followed should be present" do
  	@relationship.followed_id = nil
    assert_not @relationship.valid?
  end

  test "should follow and unfollow a user" do
    michael = readers(:michael)
    anotherone  = readers(:anotherone)
    assert_not michael.following?(anotherone)
    michael.follow(anotherone)
    assert michael.following?(anotherone)
    assert anotherone.followers.include?(michael)
    michael.unfollow(anotherone)
    assert_not michael.following?(anotherone)
  end
end
