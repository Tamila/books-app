require 'test_helper'

class ReaderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = Reader.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @reader.valid?
  end

  test "name should be present" do
    @reader.name = "     "
    assert_not @reader.valid?
  end

  test "name should not be too long" do
    @reader.name = "a" * 51
    assert_not @reader.valid?
  end

  test "names should be unique" do
    duplicate_reader = @reader.dup
    duplicate_reader.email = "anotheruser@example.com"
    @reader.save
    assert_not duplicate_reader.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @reader.email = valid_address
      assert @reader.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @reader.email = invalid_address
      assert_not @reader.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_reader = @reader.dup
    duplicate_reader.email = @reader.email.upcase
    duplicate_reader.name = "anotherone"
    @reader.save
    assert_not duplicate_reader.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @reader.email = mixed_case_email
    @reader.save
    assert_equal mixed_case_email.downcase, @reader.reload.email
  end
=begin
#Ihor Kroosh broke it
  test "password should be present (nonblank)" do
    @reader.password = @reader.password_confirmation = " " * 6
    assert_not @reader.valid?
  end
=end
  test "password should have a minimum length" do
    @reader.password = @reader.password_confirmation = "a" * 5
    assert_not @reader.valid?
  end

  test "default number of books is 0" do
    @reader.save
    assert_equal @reader.reload.book_count, 0
  end

end
