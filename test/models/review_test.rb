require 'test_helper'

class ReviewTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = Reader.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    @reader.save
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    @review = Review.new(reader: @reader, book: @book, text: "Very good book!")
  end

  test "should be valid" do
    assert @review.valid?
  end

  test "reader should be present" do
    @review.reader = nil
    assert_not @review.valid?
  end

  test "book should be present" do
    @review.book = nil
    assert_not @review.valid?
  end

  test "text should be present" do
    @review.text = nil
    assert_not @review.valid?
  end

end