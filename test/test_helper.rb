require 'simplecov'
SimpleCov.start do 
  add_filter '/config/'
  add_filter '/db/'
  add_filter '/test/'

  add_group 'Controllers', 'app/controllers'
  add_group 'Models', 'app/models'
  add_group 'Helpers', 'app/helpers'
end

#Rails.application.eager_load!

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__) 
require 'rails/test_help' 

class ActiveSupport::TestCase 
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order. 
  fixtures :all

  # Add more helper methods to be used by all tests here... 
  def is_logged_in?
    !session[:reader_id].nil?
  end

  # Log in as a particular user.
  def log_in_as(reader)
    session[:reader_id] = reader.id
  end

end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(reader, password: 'password')
    post login_path, params: { session: { email: reader.email,
                                          password: password } }
  end
end