require 'test_helper'

class ReadersLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = readers(:michael)
  end


  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: "", password: "" } }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "logged in user redirected to profile" do
  	get signup_path
  	post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }

    follow_redirect!
    get root_url
    follow_redirect!
    assert_template 'readers/show'
  end
end
