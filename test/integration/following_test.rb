require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest

  def setup
    @reader = readers(:michael)
    @other = readers(:anotherone)
    @th = readers(:notreading)
    log_in_as(@reader) #NEEDS CHANGES
  end

  test "if logged in followers page" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/readers/#{@reader.id}/foll"
    assert_not @reader.followers.empty?
    assert_match @reader.followers.count.to_s, response.body
    @reader.followers.each do |reader|
      assert_select "a[href=?]", reader_path(reader)
    end
  end

  test "if not logged in followers page" do
    get "/readers/#{@reader.id}/foll"
    assert_redirected_to root_url
  end

  test "if not logged in following page" do
    get "/readers/#{@reader.id}/foll?commit=Followings"
    assert_redirected_to root_url
  end

  test "if logged in following page" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/readers/#{@reader.id}/foll?commit=Followings"
    assert_not @reader.following.empty?
    assert_match @reader.following.count.to_s, response.body
    @reader.following.each do |reader|
      assert_select "a[href=?]", reader_path(reader)
    end
  end

  test "message if 0 followers" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/readers/#{@th.id}/foll"
    assert @th.followers.empty?
    assert_select "p#nofount"
  end


  test "should follow a reader the standard way" do
      #post relationships_path, params: { followed_id: @other.id, follower_id: @reader.id }
    @reader.follow(@other)
    assert_equal @reader.reload.following.count, 2
  end

  test "should unfollow a reader the standard way" do
    @reader.follow(@other)
    relationship = @reader.active_relationships.find_by(followed_id: @other.id)
    assert_difference '@reader.following.count', -1 do
      #delete relationship_path(relationship)
      @reader.unfollow(@other)
    end
  end

end