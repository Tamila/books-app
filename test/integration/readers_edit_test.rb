require 'test_helper'

class ReadersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @reader = readers(:michael)
    @other_reader = readers(:archer)

  end

  test "unsuccessful edit" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get edit_reader_path(@reader)
    assert_template 'readers/edit'
    patch reader_path(@reader), params: { reader: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'readers/edit'
  end


  test "successful edit with friendly forwarding" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }

    get edit_reader_path(@reader)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch reader_path(@reader), params: { reader: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_redirected_to @reader 
    @reader.reload
    assert_equal name,  @reader.name
    assert_equal email, @reader.email
  end

  test "should redirect edit when not logged in" do
    get edit_reader_path(@reader)
    assert_redirected_to root_url
  end

  test "should redirect update when not logged in" do
    patch reader_path(@reader), params: { reader: { name: @reader.name,
                                              email: @reader.email } }
    assert_redirected_to root_url
  end

  test "should redirect edit when logged in as wrong reader" do
    log_in_as(@other_reader)
    get edit_reader_path(@reader)
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong reader" do
    log_in_as(@other_reader)
    patch reader_path(@reader), params: { reader: { name: @reader.name,
                                              email: @reader.email } }
    assert_redirected_to root_url #wtf is going on here?
  end
end
