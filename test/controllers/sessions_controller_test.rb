require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @reader = readers(:michael)
  end

  test "should get new" do
    get login_url
    assert_response :success
  end

  test "login should have correct title" do
  	get login_url
  	assert_select "title", "Log in | Books App"
  end

  test "signup should have correct title" do
  	get signup_url
  	assert_select "title", "Sign up | Books App"
  end  

end
