require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest

  def setup
    @reader = readers(:michael)
    @other_reader = readers(:archer)
  end

  test "should get index" do 
    get books_url 
    assert_response :success 
    assert_template 'books/index'
  end

  test "index should have correct title" do
  	get books_url
  	assert_select "title", "Books | Books App"
  end

  test "should find exisiting book" do
    get books_url(search: 'hero')
    assert_select 'div#notfoundmessage', { :count => 0 }
  end

  test "shouldn't find unexisting book" do
    get books_url(search: 'herowhcihisunfindable')
    assert_select 'div#notfoundmessage'
  end

  test " unloggedin user shouldn't get new" do
    get new_book_path
    assert_redirected_to root_url
  end

  test "simple user shouldn't get new" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_book_path
    assert_redirected_to root_url
  end

  test "admin should get new" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_book_path
    assert_response :success
    assert_template 'books/new'
  end

  test " unloggedin user shouldn't get edit" do
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    get edit_book_path(@book)
    assert_redirected_to root_url
  end

  test "simple user shouldn't get edit" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    get edit_book_path(@book)
    assert_redirected_to root_url
  end

  test "admin should get edit" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    get edit_book_path(@book)
    assert_response :success
  end

  test "book page should be loaded and have correct title" do
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    get book_path(@book)
    assert_response :success
    assert_template 'books/show'
    assert_select "title", @book.name + " | Books App"
  end

  test "unloggedin user shouldn't get destroy" do
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    delete book_url(@book)
    assert_redirected_to root_url
  end

  test "simple user shouldn't get destroy" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    delete book_url(@book)
    assert_redirected_to root_url
  end

  test "admin should get destroy" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @book = Book.new(name: "Example book", year: 2000,
      ISBN: "65675456754", genre: "Fantasy")
    @book.save
    delete book_url(@book)
    assert_redirected_to books_url
  end

  test "admin can create a book" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/books/new"
    assert_response :success
   
    post "/books",
      params: { book: { name: "can create", year: 1995, authors: ''} }

    assert_redirected_to book_path(Book.last)
  end

  test "other reader can't create a book" do

    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/books/new"

    assert_redirected_to root_url
  end       

  test "should redirect my books when not logged in" do
    get "/readers/#{@reader.id}/mybooks"
    assert_redirected_to root_url
  end

  test "should get my books when logged in" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/readers/#{@reader.id}/mybooks"
    assert_not @reader.reader_books.empty?
    assert_match @reader.reader_books.count.to_s, response.body
    #@reader.reader_books.each do |book|
    # assert_select "a[href=?]", book_path(book)
    #end
  end

  test "should redirect recom when not logged in" do
    get "/readers/#{@reader.id}/recom"
    assert_redirected_to root_url
  end

  test "should get recom when logged in" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get "/readers/#{@reader.id}/recom"
    @rec = []
    @reader.following.each{|f|
      @rec << f.recommendations
    }
    assert_match @rec.count.to_s, response.body
    #@rec.each do |reco|
    # assert_select "a[href=?]", book_path(reco.book)
    #end
  end
end
