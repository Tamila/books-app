require 'test_helper'

class AuthorsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @reader = readers(:michael)
    @other_reader = readers(:archer)
  end

  test "should get index" do 
    get authors_url 
    assert_response :success 
  end

  test "template on index" do
    get authors_url 
    assert_template 'authors/index'
  end

  test "index should have correct title" do
  	get authors_url
  	assert_select "title", "Authors | Books App"
  end

  test "should find exisiting author" do
    get authors_url(search: 'Rob')
    assert_select 'div#notfoundmessage', { :count => 0 }
  end

  test "shouldn't find unexisting author" do
    get authors_url(search: 'herowhcihisunfindable')
    assert_select 'div#notfoundmessage'
  end

  test " unloggedin user shouldn't get new" do
    get new_author_path
    assert_redirected_to root_url
  end

  test "simple user shouldn't get new" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_author_path
    assert_redirected_to root_url
  end

  test "admin user should get new" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_author_path
    assert_response :success
    assert_template 'authors/new'
  end

  test " unloggedin user shouldn't get edit" do
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    get edit_author_path(@author)
    assert_redirected_to root_url
  end

  test "simple user shouldn't get edit" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    get edit_author_path(@author)
    assert_redirected_to root_url
  end

  test "admin should get edit" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    get edit_author_path(@author)
    assert_response :success
  end

  test "author page should be loaded and have correct title" do
    @author = authors(:salvatore)
    get author_path(@author)
    assert_response :success
    assert_select "title", @author.name + " | Books App"
    assert_template 'authors/show'
  end

  test "author book should be found" do
    @author = authors(:salvatore)
    assert_equal @author.books.count, 1
    get author_path(@author, search: 'ero')
    assert_select "div.table_items", { :count => 1 }
    assert_select 'div#notfoundmessage', { :count => 0 }
  end

  test "unfoundable not found" do
    @author = authors(:salvatore)
    get author_path(@author, search: 'heroonheroine')
    assert_select "div.table_items", { :count => @author.books.count }
    assert_select 'p#notfoundmessage'
  end

  test "unloggedin user shouldn't get destroy" do
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    delete author_url(@author)
    assert_redirected_to root_url
  end

  test "simple user shouldn't get destroy" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    delete author_url(@author)
    assert_redirected_to root_url
  end

  test "admin should get destroy" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
    delete author_url(@author)
    assert_redirected_to authors_url
  end

  test "unloggedin shouldn't update author" do
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
   
    patch author_url(@author), params: { author: { name: "updated" } }
   
    assert_redirected_to root_url
  end

  test "admin should update author" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @author = Author.new(name: "Example Author", country: "Zimbabwe",
      website: "http://www.example.com", bio: "born somewhere sometime...")
    @author.save
   
    patch author_url(@author), params: { author: { name: "updated" } }
   
    assert_redirected_to author_path(@author)
    # Reload association to fetch updated data and assert that name is updated.
    @author.reload
    assert_equal "updated", @author.name
  end

end
