require 'test_helper'

class RelationshipsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # en

  def setup
    @reader = readers(:michael)
    @other_reader = readers(:archer)
  end

  test "should redirect following when not logged in" do
    get "/readers/#{@reader.id}/foll"
    assert_redirected_to root_url
  end

  test "create should require logged-in user" do
    assert_no_difference 'Relationship.count' do
      post relationships_path, params: {relationship: { reader_id: @other_reader } }
    end
    assert_redirected_to root_url
  end
end
