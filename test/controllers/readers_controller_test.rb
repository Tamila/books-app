require 'test_helper'

class ReadersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @reader       = readers(:notreading)
    @other_reader = readers(:archer)
  end

  test "should get index if logged in" do 
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get readers_url 
    assert_response :success 
    assert_template 'readers/index'
  end

  test "should redirect index when not logged in" do
    get readers_path
    assert_redirected_to root_url
  end

  test "shouldn't get index if not logged in" do 
    get readers_url 
    assert_redirected_to root_url
  end

  test "index should have correct title and links" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
  	get readers_url
  	assert_select "title", "Readers | Books App"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", signup_path, count: 0
    assert_select "a[href=?]", logout_path
  end

  test "should find exisiting reader" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get readers_url(search: 'chael')
    assert_select 'div#notfoundmessage', { :count => 0 }
  end

  test "shouldn't find unexisting reader" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get readers_url(search: 'readerwhcihisunfindable')
    assert_select 'div#notfoundmessage'
  end

  test "unloggedin should get new" do
    get  new_reader_path
    assert_response :success
    assert_template 'readers/new'
  end

  test "loggedin shouldn't get new" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_reader_path
    assert_redirected_to root_url
  end

  test "should get reader page if logged in" do 
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @reader = readers(:michael)
    get reader_path(@reader)
    assert_response :success 
    assert_template 'readers/show'
  end

  test "shouldn't get reader page if not logged in" do 
    get readers_url(@reader)
    assert_redirected_to root_url
  end

  test "reader page should have correct title and links" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    @reader = readers(:michael)
    get reader_path(@reader)
    assert_select "title", @reader.name + " | Books App"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", signup_path, count: 0
    assert_select "a[href=?]", logout_path
    #assert_select "a[href=?]", reader_path(@current_reader)
  end

  test "should redirect destroy when not logged in" do
    #assert_no_difference 'Reader.count' do
    #  delete reader_path(@reader)
    #end
    c1 = Reader.count
    delete reader_path(@reader)
    assert_equal c1, Reader.count
    assert_redirected_to root_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    get signup_path
    post readers_path, params: { reader: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_book_path
    assert_no_difference 'Reader.count' do
      delete reader_path(@reader)
    end
    assert_redirected_to root_url
  end

  test "should їdestroy when logged in as an admin" do
    get signup_path
    post readers_path, params: { reader: { name:  "admin",
                                         email: "ad@min.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    get new_book_path
    assert_difference 'Reader.count', -1 do
      delete reader_path(@reader)
    end
    assert_redirected_to readers_path
  end

end
