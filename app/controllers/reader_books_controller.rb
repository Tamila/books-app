class ReaderBooksController < ApplicationController

def create
    puts params 
    puts params[:commit]

    if (["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]).include?(params[:commit])
        @reader_book = current_reader.reader_books.find_by!(book: reader_book_params[:book_id])
        @reader_book.rating = params[:commit];    
    elsif current_reader.reader_books.find_by(book: reader_book_params[:book_id])
        @reader_book = current_reader.reader_books.find_by!(book: reader_book_params[:book_id])
        @reader_book.status = params[:commit]
    else
        @reader_book = current_reader.reader_books.build(reader_book_params)    
        @reader_book.status = params[:commit]
    end

    #byebug
	if @reader_book.save
        redirect_back(fallback_location: root_path)
    else
    	redirect_to login_path
    end
end

def destroy
end

private

    def reader_book_params
      params.require(:reader_book).permit(:book_id, :commit)
    end

end
