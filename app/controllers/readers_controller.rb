class ReadersController < ApplicationController
  before_action :set_reader, only: [:show, :edit, :update, :destroy]

  def comCount(current_reader)
    @common = 0
    @reader.books.each {|x|
      if current_reader.books.include?(x)
        @common += 1
      end 
    }  
    return @common
  end

  def index
    if !logged_in?
      store_location
      redirect_to root_path
      return
    end
    @readers = Reader.all
    @found = true
    if params[:search]
      @readers = Reader.search(params[:search]).order("name")
      if @readers.empty?
        @found = false
        @readers = Reader.all.order('name')
      end
    else
      @readers = Reader.all.order('name')
    end
  end

  def new
    if logged_in?
      redirect_to root_path 
      return
    end
  	@reader = Reader.new
  end

  def foll
    if !logged_in?
      store_location
      redirect_to root_path 
      return
    end
    @reader = Reader.find(params[:id])
    @readers = []
    @fount = true
    @found = true

    if (['Followings']).include?(params[:commit])
      Reader.all.each { |r| 
        if @reader.following?(r)
          @readers << r;
        end
      }
    else
      Reader.all.each { |r| 
        if r.following?(@reader)
          @readers << r;
        end
      }
    end


    if @readers.empty?
      @fount = false        #я не ошибся, там правла t
    end
    @save_readers = @readers.clone

      if params[:search]
        @readers.each { |x|
          if !x.name.include?(params[:search])
            @readers.delete(x)
          end
        }
        if @readers.empty?
          @found = false
          @readers = @save_readers.clone
        end
      end
  end

  def root
    redirect_to login_readers_path
  end
  
  def show
    if !logged_in?
      store_location
      redirect_to root_path 
      return
    end
    @reader = Reader.find(params[:id])
  end

  def edit
    if check==0
      return
    end
    @reader = Reader.find(params[:id])
    if !logged_in?
      store_location
      redirect_to root_path 
    elsif current_reader.name != "admin" and current_reader.name != @reader.name
      redirect_to root_path 
    end
  end

  def update
    if check==0
      return
    end
    @reader = Reader.find(params[:id])
 
    if @reader.update(reader_params)
          redirect_to @reader
    else
          render 'edit'
    end
  end

  def create
    @reader = Reader.new(reader_params)
 
      if @reader.save
        log_in @reader
        redirect_to @reader
      else
        render 'new'
      end
  end

  def destroy
    if check==0
      return
    end

    @reader = Reader.find(params[:id])
    @reader.destroy
 
    redirect_to readers_path
  end
 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reader
      @reader = Reader.find(params[:id])
    end

    def reader_params
      params
        .require(:reader)
        .permit(:name, :email, :password, :password_confirmation)                 
    end

    def check
      if !logged_in?
        store_location
        redirect_to root_path 
        return 0
      elsif current_reader.name != "admin" and current_reader.name != @reader.name
        redirect_to root_path 
        return 0
      else
        return 1
      end
    end
end
