class RelationshipsController < ApplicationController

def create
    if !logged_in? or current_reader.id == relationship_params[:reader_id]
        store_location
        redirect_to root_path 
        return 
    end
    #Да, криво. Но красиво же работает! Хорошее решение!
    @reader = Reader.new
    @reader.id = relationship_params[:reader_id]
	
    #if @reader.id == current_reader.id  #check for following yourself
    #    redirect_to root_path 
    #    return 
    #end

    @relationship = current_reader.follow(@reader)
    if @relationship.save
        redirect_back(fallback_location: root_path)
    else
    	redirect_to login_path
    end
end

def update
    if !logged_in?
        store_location
        redirect_to root_path 
        return 
    end
    @reader = Reader.new
    @reader.id = relationship_params[:reader_id]
    current_reader.unfollow(@reader)             
 
    redirect_back(fallback_location: root_path)
end

private

    def relationship_params
      params.require(:relationship).permit(:reader_id, :commit)
    end

end
