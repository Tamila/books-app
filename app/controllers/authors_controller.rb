class AuthorsController < ApplicationController
  before_action :set_author, only: [:show, :edit, :update, :destroy]

  def index
    @authors = Author.all
    @found = true
    if params[:search]
      @authors = Author.search(params[:search]).order("name")
      if @authors.empty?
        @found = false
        @authors = Author.all.order('name')
      end
    else
      @authors = Author.all.order('name')
    end
  end


  # GET /authors/new
  def new
    if check==0
      return
    end
    @author = Author.new
  end



	def show
		@author = Author.find(params[:id])

    @books = []
    Book.all.each { |b| 
      if b.author_books.find_by(author: @author)
        @books << b;
      end
    }
    @save_books = @books.clone 

    @found = true
      if params[:search]
        @books.each { |x|
          if !x.name.include?(params[:search])
            @books.delete(x)
          end
        }
        if @books.empty?
          @found = false
          @books = @save_books
        end
      end
	end

	def edit
    if check==0
      return
    end
    	@author = Author.find(params[:id])
  end



  def update
    if check==0
      return
    end
    @author = Author.find(params[:id])
    if @author.update(author_params)
      redirect_to @author
    else
      render 'edit'
    end
  end

	def create
    if check==0
      return
    end
		@author = Author.new(author_params)
  	
    if @author.save
  		redirect_to @author
  	else
  		render 'new'
  	end
	end

	def destroy
    if check==0
      return
    end
    @author = Author.find(params[:id])
    @author.destroy
 
	  redirect_to authors_path
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_author
      @author = Author.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def author_params
      params.require(:author).permit(:name, :website, :country, :bio, :rating_average)
    end
 
    def check
      if !logged_in?
        store_location
        redirect_to root_path 
        return 0
      elsif current_reader.name != "admin"
        redirect_to root_path 
        return 0
      else
        return 1
      end
    end
end
