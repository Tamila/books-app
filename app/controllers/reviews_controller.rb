class ReviewsController < ApplicationController
before_action :logged_in?, only: [:create, :destroy]

def create
    puts params

    #@cur_book = Book.find(params[:book_id])
	@review = current_reader.reviews.build(review_params)
    #byebug
    if @review.save
    	#flash[:success] = "Micropost created!"
        redirect_back(fallback_location: root_path)
    else
    	redirect_to login_path
    end
end

def destroy
end

private

    def review_params
      #params.require(:book).permit(:id, :reviews, :reviews => [:text] )
      params.require(:review).permit(:text, :book_id)
    end

end
