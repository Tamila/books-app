class RecommendationsController < ApplicationController
def create
    puts params

	@recommendation = current_reader.recommendations.build(recommendation_params)
    if @recommendation.save
    	#flash[:success] = "Micropost created!"
        redirect_back(fallback_location: root_path)
    else
    	redirect_to login_path
    end
end

def destroy
end

private

    def recommendation_params
      #params.require(:book).permit(:id, :reviews, :reviews => [:text] )
      params.require(:recommendation).permit(:book_id)
    end

end
