class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  def index
    @books = Book.all
    @found = true
    if params[:search]
      @books = Book.search(params[:search]).order("name")
      if @books.empty?
        @found = false
        @books = Book.all.order('name')
      end
    else
      @books = Book.all.order('name')
    end
  end

  def recom
    if !logged_in?
      store_location
      redirect_to root_path
      return
    end
    @reader = Reader.find(params[:id])
    @books = []
    Reader.all.each{ |r|
      if @reader.following?(r)
        @read = r
        Book.all.each { |b| 
          if b.recommendations.find_by(reader: r)
            if !current_reader.reader_books.find_by(book: b)
              @books << b;
            end
          end
        }
      end
    }

    @save_books = @books.clone
    @found = true
      if params[:search]
        @books.each { |x|
          if !x.name.include?(params[:search])
            @books.delete(x)
          end
        }
        if @books.empty?
          @found = false
          @books = @save_books
        end
      end
  end

  def mybooks
    if !logged_in?
      store_location
      redirect_to root_path
      return
    end
    @reader = Reader.find(params[:id])
    @books = []
    @fount = true
    if (['Want to read', 'Read', 'Reading']).include?(params[:commit])
      Book.all.each { |b| 
        if b.reader_books.find_by(reader: @reader)
          if b.reader_books.find_by!(reader: @reader).status == params[:commit]
            @books << b;
          end
        end
      }

      if @books.empty?
        @fount = false        #я не ошибся, там правла t
        Book.all.each { |b| 
          if b.reader_books.find_by(reader: @reader)
            @books << b;
          end
        }
      end
    else
      Book.all.each { |b| 
        if b.reader_books.find_by(reader: @reader)
          @books << b;
        end
      }
    end

    @save_books = @books.clone
    @found = true
      if params[:search]
        @books.each { |x|
          if !x.name.include?(params[:search])
            @books.delete(x)
          end
        }
        if @books.empty?
          @found = false
          @books = @save_books
        end
      end
  end

  def new
    if check==0
      return
    end
  	@book = Book.new
    @authors = Author.all
  end 

  def show
    @book = Book.find(params[:id])
    @authors = Author.all
    @review = current_reader.reviews.build if logged_in?

  end

  def edit
    if check==0
      return
    end
    @book = Book.find(params[:id])
  end

  def update
    if check==0
      return
    end
    @book = Book.find(params[:id])

    Author.all.each { |a| 
      if a.author_books.find_by(book: @book)
          a.author_books.find_by!(book: @book).destroy
      end
    }

    if @book.update(book_params)
        @author = adv_params[:authors]
        @author = @author.split(',')
        @author.each {|each_auth|
          Author.all.each { |a| 
            if a.name == each_auth
              @author_book = @book.author_books.build(author: a)
              if !@author_book.save
                redirect_to root_path
              end
            end
          }
        }
        redirect_to @book
      else
        render 'edit'
      end
  end

  def create
    if check==0
      return
    end
    @book = Book.new(book_params)

      if @book.save
        @author = adv_params[:authors]
        @author = @author.split(',')
        @author.each {|each_auth|
          Author.all.each { |a| 
            if a.name == each_auth
              @author_book = @book.author_books.build(author: a)
              if !@author_book.save
                redirect_to root_path
              end
            end
          }
        }
        redirect_to @book
      else
        render 'new'
      end
  end

  def destroy
    if check==0
      return
    end
    @book = Book.find(params[:id])
    @book.destroy
 
    redirect_to books_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:name, :year, :ISBN, :genre, :rating_amount, :rating_average)
    end

    def adv_params
        params.require(:book).permit(:name, :year, :ISBN, :genre, :rating_amount, :rating_average, :authors)
    end

    def check
      if !logged_in?
        store_location
        redirect_to root_path 
        return 0
      elsif current_reader.name != "admin"
        redirect_to root_path 
        return 0
      else
        return 1
      end
    end
end
