class SessionsController < ApplicationController
  def new
    if logged_in?
      redirect_to reader_path(current_reader)
    end
    #else
    #  redirect_to login_path
    #end
  end

  def root
    if logged_in?
      redirect_to reader_path(current_reader)
    else
      redirect_to login_path
    end
  end
  def create
    reader = Reader.find_by(email: params[:session][:email].downcase)
    if reader && reader.authenticate(params[:session][:password_digest])
      # Log the user in and redirect to the user's show page.
      log_in reader
      redirect_back_or reader
      #redirect_to reader
    else
      # Create an error message.
      flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to login_path
  end
end
