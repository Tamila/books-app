class AuthorBook < ApplicationRecord
  after_save :avg_author

  def avg_author
    @author = self.author
    @book = Array.new
    AuthorBook.all.each {
        |x| if x.author == @author
              @book << x.book
            end
    }

    @total = 0
    @avg = 0
    @amount = 0
    @book.each {
      |a|
      @total += a.rating_average * a.rating_amount
      @amount += a.rating_amount
    }
    @avg = @total.to_f / @amount.to_f
    @authorTMP = Author.find(@author)
    @authorTMP.rating_average = @avg
    @authorTMP.save
  end

  belongs_to :book
  belongs_to :author

  validates :book, presence: true
  validates :author, presence: true
end
