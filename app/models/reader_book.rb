class ReaderBook < ApplicationRecord
  after_initialize :set_defaults, unless: :persisted?
  after_save :avg_book_update, :avg_author_update, :reader_update_counter


  
  def avg_book_update
    
    if self.rating == 0
      return
    end

    @tmp = 0
    @rating_amount = 0
    @book = self.book

    ReaderBook.all.each {
        |x| if x.book == @book
              @tmp += x.rating
              @rating_amount += 1 if x.rating != 0
            end
    }

    @rating_average = @tmp.to_f / @rating_amount.to_f
    @bk = Book.find(@book)
    @bk.rating_amount = @rating_amount
    @bk.rating_average = @rating_average
    @bk.save
  end

  def avg_author_update
    if self.rating == 0
      return
    end
    @book = self.book
    @author = Array.new

    AuthorBook.all.each {
        |x| if x.book == @book
              @author << x.author
            end
    }

    @author.each {
      |a|
      @total = 0
      @avg = 0
      @amount = 0
      @authorBook = Array.new
      AuthorBook.all.each{
        |x| if x.author == a
              @authorBook << x.book
            end
      }
      @authorBook.each{
        |x| @total += x.rating_average * x.rating_amount
        @amount += x.rating_amount
      }
      @avg = @total.to_f / @amount.to_f
      @authorTMP = Author.find(a)
      @authorTMP.rating_average = @avg
      @authorTMP.save
    }
  end

  def reader_update_counter
    @book_count = 0
    @reader = self.reader

    ReaderBook.all.each {
        |x| @book_count += 1 if x.reader == @reader
    }
    @r = Reader.find(@reader)
    @r.book_count = @book_count
    @r.save
  end

  def set_defaults
    self.rating  ||= 0
  end

  belongs_to :book
  belongs_to :reader

  validates :status,  presence: true, 
  					inclusion: { in: ['Read', 'Reading', 'Want to read']}
  validates :rating,  presence: true, inclusion: { :in => 0..10}
end
