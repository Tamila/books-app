class Relationship < ApplicationRecord
  belongs_to :follower, class_name: "Reader"
  belongs_to :followed, class_name: "Reader"
  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
