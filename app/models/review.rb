class Review < ApplicationRecord
  belongs_to :book
  belongs_to :reader

  validates :reader, presence: true
  default_scope -> { order(created_at: :desc) }
  validates :book, presence: true
  validates :text, presence: true
end
