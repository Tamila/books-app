class Author < ApplicationRecord
  after_initialize :set_defaults, unless: :persisted?
  # The set_defaults will only work if the object is new

  def set_defaults
    self.rating_average  ||= 0
  end

	has_many :author_books
  has_many :books, through: :author_books


  validates :name, presence: true, length: { maximum: 100 }
  validates_format_of :website, :allow_blank => true, :with => URI::regexp


	def self.search(search)
	  where("name ILIKE ?", "%#{search}%") 
	end
end
