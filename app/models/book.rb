class Book < ApplicationRecord
  after_initialize :set_defaults, unless: :persisted?
  # The set_defaults will only work if the object is new

  def set_defaults
    self.rating_amount  ||= 0
    self.rating_average  ||= 0
  end

  def self.search(search)
    where("name ILIKE ?", "%#{search}%") 
  end

  has_many :reader_books
  has_many :readers, through: :reader_books

  has_many :reviews, dependent: :destroy
  has_many :readers, through: :reviews

  has_many :recommendations, dependent: :destroy
  has_many :readers, through: :recommendations

  has_many :author_books
  has_many :authors, through: :author_books

  	validates :name, presence: true, length: {maximum: 255}
    validates :year, :allow_blank => true, inclusion: { in: 1000..Date.today.year } #умовно від 1000 року видання

  	validates :ISBN, :allow_blank => true, uniqueness: true
end
