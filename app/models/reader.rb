class Reader < ApplicationRecord
  after_initialize :set_defaults, unless: :persisted?
  # The set_defaults will only work if the object is new

  def comCount(current_reader)
    @common = 0
    self.reader_books.each {|x|
      b = x.book
      if ReaderBook.find_by(reader: current_reader, book: b)
        @common += 1
      end 
    }  
    return @common
  end

  def set_defaults
    self.book_count  ||= 0
  end

  def self.search(search)
    where("name ILIKE ?", "%#{search}%") 
  end

  has_many :reader_books
  has_many :books, through: :reader_books

  has_many :reviews, dependent: :destroy
  has_many :books, through: :reviews

  has_many :recommendations, dependent: :destroy
  has_many :books, through: :recommendations

  has_many :active_relationships, class_name:  "Relationship",
           foreign_key: "follower_id",
           dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
           foreign_key: "followed_id",
           dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  before_save { email.downcase! }
  
  validates :name,  presence: true, length: { maximum: 50 }, uniqueness: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
             uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, if: ->(r) { r.password.present? }

   # Returns the hash digest of the given string.
  def Reader.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
