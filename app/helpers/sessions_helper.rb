module SessionsHelper

	# Logs in the given user.
  def log_in(reader)
    session[:reader_id] = reader.id
  end

  # Returns the current logged-in user (if any).
  def current_reader
    @current_reader ||= Reader.find_by(id: session[:reader_id])
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_reader.nil?
  end

  # Logs out the current user.
  def log_out
    session.delete(:reader_id)
    @current_reader = nil
  end


  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
