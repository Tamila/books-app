class AddBookIdReaderIdToRecommendation < ActiveRecord::Migration[5.0]
  def change
    add_reference :recommendations, :reader, foreign_key: true
    add_reference :recommendations, :book, foreign_key: true
  end
end
