class CreateAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :bio
      t.string :country
      t.string :website
      t.float :rating_average

      t.timestamps
    end
  end
end
