class RemoveBookIdAuthorIdFromAuthorBook < ActiveRecord::Migration[5.0]
  def change
    remove_column :author_books, :bookID, :integer
    remove_column :author_books, :authorID, :integer
  end
end
