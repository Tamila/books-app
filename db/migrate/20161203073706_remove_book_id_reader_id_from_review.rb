class RemoveBookIdReaderIdFromReview < ActiveRecord::Migration[5.0]
  def change
    remove_column :reviews, :bookID, :integer
    remove_column :reviews, :readerID, :integer
  end
end
