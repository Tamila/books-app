class AddBookIdReaderIdToReview < ActiveRecord::Migration[5.0]
  def change
    add_reference :reviews, :reader, foreign_key: true
    add_reference :reviews, :book, foreign_key: true
  end
end
