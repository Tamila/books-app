class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.integer :bookID
      t.integer :readerID
      t.date :date
      t.binary :text

      t.timestamps
    end
    add_index :reviews, [:bookID, :created_at]
  end
end
