class RemoveBookIdReaderIdFromReaderBook < ActiveRecord::Migration[5.0]
  def change
    remove_column :reader_books, :bookID, :integer
    remove_column :reader_books, :readerID, :integer
  end
end
