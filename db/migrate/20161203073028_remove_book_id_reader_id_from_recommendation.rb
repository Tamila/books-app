class RemoveBookIdReaderIdFromRecommendation < ActiveRecord::Migration[5.0]
  def change
    remove_column :recommendations, :bookID, :integer
    remove_column :recommendations, :readerID, :integer
  end
end
