class AddBookIdReaderIdToReaderBook < ActiveRecord::Migration[5.0]
  def change
    add_reference :reader_books, :reader, foreign_key: true
    add_reference :reader_books, :book, foreign_key: true
  end
end
