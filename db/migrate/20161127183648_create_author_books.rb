class CreateAuthorBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :author_books do |t|
      t.integer :bookID
      t.integer :authorID

      t.timestamps
    end
  end
end
