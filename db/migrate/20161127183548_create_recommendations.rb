class CreateRecommendations < ActiveRecord::Migration[5.0]
  def change
    create_table :recommendations do |t|
      t.integer :bookID
      t.integer :readerID

      t.timestamps
    end
  end
end
