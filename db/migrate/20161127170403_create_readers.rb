class CreateReaders < ActiveRecord::Migration[5.0]
  def change
    create_table :readers do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.integer :book_count

      t.timestamps
    end
  end
end
