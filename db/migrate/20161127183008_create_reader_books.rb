class CreateReaderBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :reader_books do |t|
      t.integer :bookID
      t.integer :readerID
      t.string :status
      t.integer :rating

      t.timestamps
    end
  end
end
