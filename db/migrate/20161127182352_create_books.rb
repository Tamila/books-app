class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :name
      t.integer :year
      t.string :ISBN
      t.string :genre
      t.integer :rating_amount
      t.float :rating_average

      t.timestamps
    end
  end
end
