--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: author_books; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE author_books (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    author_id integer,
    book_id integer
);


--
-- Name: author_books_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE author_books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: author_books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE author_books_id_seq OWNED BY author_books.id;


--
-- Name: authors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE authors (
    id integer NOT NULL,
    name character varying,
    bio character varying,
    country character varying,
    website character varying,
    rating_average double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: authors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE authors_id_seq OWNED BY authors.id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE books (
    id integer NOT NULL,
    name character varying,
    year integer,
    "ISBN" character varying,
    genre character varying,
    rating_amount integer,
    rating_average double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- Name: reader_books; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE reader_books (
    id integer NOT NULL,
    status character varying,
    rating integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    reader_id integer,
    book_id integer
);


--
-- Name: reader_books_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reader_books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reader_books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE reader_books_id_seq OWNED BY reader_books.id;


--
-- Name: readers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE readers (
    id integer NOT NULL,
    name character varying,
    email character varying,
    password_digest character varying,
    book_count integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: readers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE readers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: readers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE readers_id_seq OWNED BY readers.id;


--
-- Name: recommendations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE recommendations (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    reader_id integer,
    book_id integer
);


--
-- Name: recommendations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recommendations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recommendations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE recommendations_id_seq OWNED BY recommendations.id;


--
-- Name: relationships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE relationships (
    id integer NOT NULL,
    follower_id integer,
    followed_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: relationships_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE relationships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: relationships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE relationships_id_seq OWNED BY relationships.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE reviews (
    id integer NOT NULL,
    text bytea,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    reader_id integer,
    book_id integer
);


--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE reviews_id_seq OWNED BY reviews.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_books ALTER COLUMN id SET DEFAULT nextval('author_books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY authors ALTER COLUMN id SET DEFAULT nextval('authors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reader_books ALTER COLUMN id SET DEFAULT nextval('reader_books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY readers ALTER COLUMN id SET DEFAULT nextval('readers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY recommendations ALTER COLUMN id SET DEFAULT nextval('recommendations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY relationships ALTER COLUMN id SET DEFAULT nextval('relationships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reviews ALTER COLUMN id SET DEFAULT nextval('reviews_id_seq'::regclass);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: author_books_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_books
    ADD CONSTRAINT author_books_pkey PRIMARY KEY (id);


--
-- Name: authors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: reader_books_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reader_books
    ADD CONSTRAINT reader_books_pkey PRIMARY KEY (id);


--
-- Name: readers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY readers
    ADD CONSTRAINT readers_pkey PRIMARY KEY (id);


--
-- Name: recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recommendations
    ADD CONSTRAINT recommendations_pkey PRIMARY KEY (id);


--
-- Name: relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY relationships
    ADD CONSTRAINT relationships_pkey PRIMARY KEY (id);


--
-- Name: reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: index_author_books_on_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_author_books_on_author_id ON author_books USING btree (author_id);


--
-- Name: index_author_books_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_author_books_on_book_id ON author_books USING btree (book_id);


--
-- Name: index_reader_books_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_reader_books_on_book_id ON reader_books USING btree (book_id);


--
-- Name: index_reader_books_on_reader_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_reader_books_on_reader_id ON reader_books USING btree (reader_id);


--
-- Name: index_recommendations_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_recommendations_on_book_id ON recommendations USING btree (book_id);


--
-- Name: index_recommendations_on_reader_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_recommendations_on_reader_id ON recommendations USING btree (reader_id);


--
-- Name: index_relationships_on_followed_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_relationships_on_followed_id ON relationships USING btree (followed_id);


--
-- Name: index_relationships_on_follower_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_relationships_on_follower_id ON relationships USING btree (follower_id);


--
-- Name: index_relationships_on_follower_id_and_followed_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_relationships_on_follower_id_and_followed_id ON relationships USING btree (follower_id, followed_id);


--
-- Name: index_reviews_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_reviews_on_book_id ON reviews USING btree (book_id);


--
-- Name: index_reviews_on_reader_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_reviews_on_reader_id ON reviews USING btree (reader_id);


--
-- Name: fk_rails_18262e567c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recommendations
    ADD CONSTRAINT fk_rails_18262e567c FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: fk_rails_1d324bbec7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT fk_rails_1d324bbec7 FOREIGN KEY (reader_id) REFERENCES readers(id);


--
-- Name: fk_rails_38b88c8b78; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recommendations
    ADD CONSTRAINT fk_rails_38b88c8b78 FOREIGN KEY (reader_id) REFERENCES readers(id);


--
-- Name: fk_rails_80ea7f00b1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_books
    ADD CONSTRAINT fk_rails_80ea7f00b1 FOREIGN KEY (author_id) REFERENCES authors(id);


--
-- Name: fk_rails_924a0b30ca; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT fk_rails_924a0b30ca FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: fk_rails_a3bd73350d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_books
    ADD CONSTRAINT fk_rails_a3bd73350d FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: fk_rails_c634ad8804; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reader_books
    ADD CONSTRAINT fk_rails_c634ad8804 FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: fk_rails_ee9b59e188; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reader_books
    ADD CONSTRAINT fk_rails_ee9b59e188 FOREIGN KEY (reader_id) REFERENCES readers(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20161127170403'), ('20161127182352'), ('20161127182739'), ('20161127183008'), ('20161127183141'), ('20161127183548'), ('20161127183648'), ('20161203065919'), ('20161203070202'), ('20161203073028'), ('20161203073252'), ('20161203073706'), ('20161203073911'), ('20161203074125'), ('20161203074239'), ('20161212101555'), ('20161212110637');


