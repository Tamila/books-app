Rails.application.routes.draw do


  root 'sessions#root'
  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'  
  get '/signup', to: 'readers#new'
  post '/signup', to: 'readers#create'

  get '/readers/:id/recom', to: 'books#recom'
  get '/readers/:id/foll', to: 'readers#foll'
  post '/readers/:id/foll', to: 'readers#foll'
  get '/readers/:id/mybooks', to: 'books#mybooks'
  post '/readers/:id/mybooks', to: 'books#mybooks'


  resources :author_books
  resources :reader_books
  resources :recommendations
  resources :relationships
  resources :readers
  resources :authors
  resources :books
  resources :reviews,          only: [:create, :destroy]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
