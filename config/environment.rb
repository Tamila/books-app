# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

ENV['FIXTURES'] ||= 'books, readers, authors, relationships, reader_books, author_books'