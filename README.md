# README

Book-app.com

Meet a place for readers of all kinds!
* Keep track of your books
* Find out what your friends are reading
* Discover new books picked just for you
* Rate, comment and discuss your favourite literature
